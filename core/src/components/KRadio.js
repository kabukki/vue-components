import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faCircle, faDotCircle } from '@fortawesome/free-regular-svg-icons';
import Colorable from '../mixins/Colorable';

export default {
	name: 'KRadio',
	inheritAttrs: false,
	mixins: [ Colorable ],
	model: {
		// Common variable between KRadios
		prop: 'common',
		event: 'change'
	},
	props: {
		disabled: Boolean,
		label: String,
		help: String,
		common: {
			type: String,
			required: true
		},
		value: {
			type: String,
			required: true
		}
	},
	computed: {
		classes () {
			return {
				'k-radio': true,
				disabled: this.disabled
			};
		},
		listeners () {
			return {
				...this.$listeners,
				change: this.onChange
			};
		},
		checked () {
			return this.value === this.common;
		},
		radioIcon () {
			return this.checked ? faDotCircle : faCircle;
		}
	},
	methods: {
		onChange () {
			this.$emit('change', this.value);
		},
		genLabel () {
			const children = [ this.genControl() ];

			if (this.label) {
				children.push(this.$createElement('span', [ this.label ]));
			}

			return this.$createElement('label', children);
		},
		genHelper () {
			return this.$createElement('small', {
				class: 'help'
			}, [ this.help ]);
		},
		genControl () {
			return this.$createElement('div', {
				class: 'control'
			}, [
				this.$createElement('input', {
					attrs: {
						...this.$attrs,
						type: 'radio'
					},
					on: this.listeners,
					domProps: {
						value: this.value,
						checked: this.checked,
						disabled: this.disabled
					}
				}),
				this.$createElement(FontAwesomeIcon, {
					props: {
						icon: this.radioIcon
					}
				})
			]);
		}
	},
	render (h) {
		const children = [ this.genLabel() ];

		if (this.help) {
			children.push(this.genHelper());
		}

		return h('div', this.setColor(this.color, {
			class: this.classes
		}), children);
	}
};
